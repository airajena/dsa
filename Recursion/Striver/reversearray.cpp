#include <iostream>
#include <vector>
using namespace std;

void solve(vector<int> &v, int s, int e) {
    if (s >= e) {
        return;
    }
    swap(v[s], v[e]);
    solve(v, s + 1, e - 1);
}

int main() {
    vector<int> v;
    int n;
    cout << "Enter the number of elements: ";
    cin >> n;

    cout << "Enter " << n << " elements: ";
    for (int i = 0; i < n; i++) {
        int num;
        cin >> num;
        v.push_back(num);
    }

    int s = 0;
    int e = v.size() - 1;

    solve(v, s, e);

    cout << "Swapped vector elements: ";
    for (int i = 0; i < v.size(); i++) {
        cout << v[i] << " ";
    }
    cout << endl;

    return 0;
}
