#include <iostream>
using namespace std;

bool isPalindrome(string str, int start, int end) {
    // Base case: if start meets or crosses end, it's a palindrome
    if (start >= end) {
        return true;
    }

    // Check if characters at start and end positions are equal
    if (str[start] != str[end]) {
        return false; // If not equal, it's not a palindrome
    }

    // Recur for the substring excluding the start and end characters
    return isPalindrome(str, start + 1, end - 1);
}

int main() {
    string input;
    cout << "Enter a string: ";
    cin >> input;

    int len = input.length();

    bool result = isPalindrome(input, 0, len - 1);

    if (result) {
        cout << "The string is a palindrome." << endl;
    } else {
        cout << "The string is not a palindrome." << endl;
    }

    return 0;
}
