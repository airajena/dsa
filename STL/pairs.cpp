#include <bits/stdc++.h>
using namespace std;

int main(){
    // Taking input of pair and printing it
    pair<int,string> p;
    cin>>p.first>>p.second;
    cout<<p.first<<" "<<p.second<<endl;

    // Another way of taking input of pair
    pair<int,int> p2(1,2);
    cout<<p2.first<<" "<<p2.second<<endl;
    
    // Another way of taking input of pair
    pair<int,int> p3 = make_pair(1,2);
    cout<<p3.first<<" "<<p3.second<<endl;
    
    // Array of pairs
    pair<int,int> arr[] = {make_pair(1,2),make_pair(3,4),make_pair(5,6)};
    cout<<arr[0].first<<" "<<arr[0].second<<endl;


}